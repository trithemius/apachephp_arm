
node {

    try {
    notifyBuild('STARTED')

    def testname = "${env.JOB_NAME}_jenk_${env.BUILD_NUMBER}"

    stage('Clone repository') {
        /* Let's make sure we have the repository cloned to our workspace */

        checkout scm

    }

    stage('clean workspace') {
        ansiblePlaybook( 
          playbook: 'clean.yaml',
          inventory: 'inventory/hosts', 
          credentialsId: '25816c20-4bf5-4d5c-b49d-6394dc4d927c')
    }

    stage('git repository') {
        ansiblePlaybook( 
          playbook: 'gitclone.yaml',
          inventory: 'inventory/hosts', 
          credentialsId: '25816c20-4bf5-4d5c-b49d-6394dc4d927c')
    }

    stage('Build image') {
        /* This builds the actual image; synonymous to
         * docker build on the command line */

        ansiblePlaybook( 
          playbook: 'build.yaml',
          inventory: 'inventory/hosts', 
          credentialsId: '25816c20-4bf5-4d5c-b49d-6394dc4d927c')
    }



    stage('Docker test'){
        ansiblePlaybook( 
          playbook: 'test.yaml',
          inventory: 'inventory/hosts', 
          credentialsId: '25816c20-4bf5-4d5c-b49d-6394dc4d927c')
    }

    stage('Push image') {
        /* Finally, we'll push the image with two tags:
         * First, the incremental build number from Jenkins
         * Second, the 'latest' tag.
         * Pushing multiple tags is cheap, as all the layers are reused. */

        ansiblePlaybook( 
          playbook: 'publish.yaml',
          inventory: 'inventory/hosts', 
          credentialsId: '25816c20-4bf5-4d5c-b49d-6394dc4d927c')
    }

    stage('Deploy image'){
        ansiblePlaybook( 
          playbook: 'deploy.yaml',
          inventory: 'inventory/hosts', 
          credentialsId: '25816c20-4bf5-4d5c-b49d-6394dc4d927c')
    }

    } catch (e) {
      currentBuild.result = "FAILED"
      throw e 
    } finally {
      notifyBuild(currentBuild.result)
    } 


}


def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary)
}
